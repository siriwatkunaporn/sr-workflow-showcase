export const CONSTANT = 102;

export const BREAKING_CHANGE = false;

export default () => {
  return {
    listPokemons: () => ["Charmander", "Pikachu"],
  };
};
