module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        modules: process.env.BABEL_ENV === 'esm' ? false : 'commonjs',
      },
    ],
    '@babel/preset-typescript',
  ],
  ignore: [
    "**/*.test.tsx",
    "**/*.test.ts",
  ]
};
