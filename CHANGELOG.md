# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.1.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.1.0-rc.0...v2.1.0) (2020-10-17)

## [2.1.0-rc.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.1-rc.0...v2.1.0-rc.0) (2020-10-17)


### Features

* new component ([be67f51](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/be67f51df16ece98daa87b77506959148091d831))

### [2.0.1-rc.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v2.0.0...v2.0.1-rc.0) (2020-10-17)


### Bug Fixes

* update constant ([c747c93](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/c747c93044195b0dbe51e02a1160aba0473aa8bb))

## [2.0.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v1.1.0-rc.0...v2.0.0) (2020-10-17)

## [2.0.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v1.1.0-rc.0...v2.0.0) (2020-10-17)

## [1.1.0-rc.0](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/compare/v1.0.0...v1.1.0-rc.0) (2020-10-17)


### Features

* add Pikachu ([d7b0a9c](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/d7b0a9cf8ebbc3d1cc019c324ccb9531c8f07994))


### Bug Fixes

* update constant ([758210a](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/758210a30a8de1892b82aa7a38fae5fcc45d6637))
* update var ([e7a833f](https://gitlab.com/siriwatkunaporn/sr-workflow-demo/commit/e7a833f24e70b868be243b4287075b63888dd3d2))

## 1.0.0 (2020-10-17)
